import RPi.GPIO as IO

IO.setwarnings(False)
IO.setmode(IO.BCM)

IR_L = 25
IR_R = 23

IO.setup(IR_L, IO.IN)  # GPIO 10 -> Left IR out
IO.setup(IR_R, IO.IN)  # GPIO 23 -> Right IR out

while True:
    sen1 = IO.input(IR_L)  # Read Left Sensor
    sen2 = IO.input(IR_R)  # Read Right Sensor

    print "Left Sensor: %d; Right Sensor: %d" % (sen1, sen2)

