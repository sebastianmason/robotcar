import RPi.GPIO as IO
from time import sleep
import socket
# Import SPI library (for hardware SPI) and MCP3008 library.
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

IO.setwarnings(False)
IO.setmode(IO.BOARD)

HIGH = True
LOW = False

maxVal = 999

HOST = '192.168.43.184'
PORT = 65432

# Hardware SPI configuration:
SPI_PORT = 0
SPI_DEVICE = 0
mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

# region Pin names
En1 = 3  # Enable display 1
En2 = 5  # Enable display 2
En3 = 8  # Enable display 3

D0 = 7  # LSB for display decoder
D1 = 11  # Bit 2 for display decoder
D2 = 13  # Bit 3 for display decoder
D3 = 15  # MSB for display decoder
# endregion

# region Pin groupings
outList = [D0, D1, D2, D3]
enList = [En1, En2, En3]
# endregion


def dec2bin(x):
    lst = []
    if x > 1:
        lst += dec2bin(x/2) + [x % 2]
    else:
        lst += [x]

    return lst

def decs2bins(lst):
    out = []
    for i in lst:
        out.append(dec2bin(i))

    return out

def setOutput(lst):
    for i in lst:
        IO.setup(i, IO.OUT)

def setInput(lst):
    for i in lst:
        IO.setup(i, IO.IN)

def setToLow(lst):
    for i in lst:
        IO.output(i, LOW)

def setToHigh(lst):
    for i in lst:
        IO.output(i, HIGH)

def addZ(lst):
    #print lst
    if len(lst) == 1:
        lst = [0, 0, 0] + lst
    elif len(lst) == 2:
        lst = [0, 0] + lst
    elif len(lst) == 3:
        lst = [0] + lst

    return lst

def setDs(x):
    x = addZ(x)
    lst = list(reversed(x))
    #print lst
    for i in range(len(lst)):
     #   print i
        if lst[i]:
            IO.output(outList[i], HIGH)
        else:
            IO.output(outList[i], LOW)

def numOut(n):
    numlst = []
    n = int(n)
#    print n
    numlst.append(n // 100)
    numlst.append((n % 100) // 10)
    numlst.append(n % 10)
#    print numlst
    return numlst

def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)

def setDsNutz(lists):
    for i in range(len(lists)):
        IO.output(enList[i], LOW)
#        print lists
        setDs(lists[i])
        sleep(0.005)
        setToHigh(enList)

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    count = 20
    while True:
        if count == 20:
            c = round(translate(mcp.read_adc(0), 0, 1023, 0, maxVal), 1)
            c2 = numOut(c)
            c3 = decs2bins(c2)
            count = 0
        print c/10
        s.sendall(str(c/10).encode("utf-8"))
        setDsNutz(c3)
        count += 1


setOutput(enList)
setOutput(outList)
main()
