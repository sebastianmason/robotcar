from socket import socket, AF_INET, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR
import thread
from time import sleep, gmtime, strftime
BUFF = 5
HOST = ''
PORT1 = 65432
PORT2 = 65433
Fdata = ['00000', '00000']


def gen_response():
    return 'this_is_the_return_from_the_server'


def handler(clientsock, addr):
    global Fdata
    while 1:
        data = clientsock.recv(BUFF)
        if addr[0] == "192.168.42.19":
            Fdata[0] = str(data)
        else:
            Fdata[1] = str(data)

        if not data:
            break


sorted(list("123"),)

dirc = {
    "0": "Forward",
    "1": "Right  ",
    "2": "Left   "
}

if __name__ == '__main__':
    ADDR = [(HOST, PORT1), (HOST, PORT2)]

    for i in range(2):
        serversock = socket(AF_INET, SOCK_STREAM)
        serversock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        serversock.bind(ADDR[i])
        serversock.listen(5)

        print 'waiting for connection...'
        clientsock, addr = serversock.accept()
        print '...connected from:', addr
        thread.start_new_thread(handler, (clientsock, addr))

    while True:
        sleep(1)
        # print Fdata
        output = "%s    Left IR sensor: %s    Rigth IR sensor: %s    Direction: %s    Battery %%: %s" % (
            strftime("%Y-%m-%d %H:%M:%S", gmtime()),
            Fdata[1][0],
            Fdata[1][1],
            Fdata[1][2],
            Fdata[0])

        print output

        with open("log.txt", "a") as f:
            f.write(output + "\n")
