import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 65400))
s.listen(5)
while True:
    print("Listening at", s.getsockname())
    try:
        sa, sockname = s.accept()
        print("We have accepted a connection from", sockname)
        break
    except Exception as e:
        print(e)


def main():
    print("heyy")
    pi_ready = str(sa.recv(5).decode("UTF-8"))
    print(pi_ready)
    if pi_ready == "ready":
        print("SebPi is ready")


def run_program():
    while True:
        start_program = input("press s to start the robot_car or q to stop. ")
        print
        if start_program == "s":
            sa.sendall("s".encode("UTF-8"))
            print("The car is running")
        elif start_program == "q":
            sa.sendall("q".encode("UTF-8"))
            print("The car stopped")


try:
    main()
    run_program()
except Exception as e:
    print(e)
    sa.close()
    s.close()
    exit()
