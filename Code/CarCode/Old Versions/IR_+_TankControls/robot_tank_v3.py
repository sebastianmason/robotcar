import RPi.GPIO as IO
from time import sleep

IO.setwarnings(False)
IO.setmode(IO.BCM)

HIGH = True
LOW = False

# region Pin config
IR_L = 25  # GPIO 25 -> Left IR out
IR_R = 23  # GPIO 23 -> Right IR out

M1_AF = 3  # GPIO 3 -> Motor 1 terminal A Front
M1_BF = 2  # GPIO 2 -> Motor 1 terminal B Front
M2_AF = 17  # GPIO 17 -> Motor 2 terminal A Front
M2_BF = 18  # GPIO 18 -> Motor 2 terminal B Front

M1_AB = 12  # GPIO 12 -> Motor 1 terminal A Back
M1_BB = 13  # GPIO 13 -> Motor 1 terminal B Back
M2_AB = 26  # GPIO 26 -> Motor 2 terminal A Back
M2_BB = 16  # GPIO 16 -> Motor 2 terminal B Back

M1_EnF = 27  # GPIO 27 -> Motor Left Enable Front
M2_EnF = 4   # GPIO 4 -> Motor Right Enable Front
M1_EnB = 5   # GPIO 5 -> Motor Left Enable Back
M2_EnB = 6   # GPIO 6 -> Motor Right Enable Back
# endregion

# region Pin groupings
outList = [M1_AF, M1_BF, M2_AF, M2_BF, M1_AB, M1_BB,
           M2_AB, M2_BB, M1_EnF, M2_EnF, M1_EnB, M2_EnB]
inList = [IR_L, IR_R]
enList = [M1_EnF, M2_EnF, M1_EnB, M2_EnB]
# endregion


def setOutput(lst):
    for i in lst:
        IO.setup(i, IO.OUT)


def setInput(lst):
    for i in lst:
        IO.setup(i, IO.IN)


def setToHigh(lst):
    for i in lst:
        IO.output(i, HIGH)


def rightSide(a):
    if a:  # Forwards
        IO.output(M1_AF, HIGH)  # 1A+ (Front)
        IO.output(M1_BF, LOW)   # 1B- (Front)
        IO.output(M1_AB, HIGH)  # 1A+ (Back)
        IO.output(M1_BB, LOW)   # 1B- (Back)
    else:  # Backwards
        IO.output(M1_AF, LOW)  # 1A+ (Front)
        IO.output(M1_BF, HIGH)   # 1B- (Front)
        IO.output(M1_AB, LOW)  # 1A+ (Back)
        IO.output(M1_BB, HIGH)   # 1B- (Back)


def leftSide(a):
    if a:  # Forwards
        IO.output(M2_AF, LOW)   # 2A- (Front)
        IO.output(M2_BF, HIGH)  # 2B+ (Front)
        IO.output(M2_AB, LOW)   # 2A- (Back)
        IO.output(M2_BB, HIGH)  # 2B+ (Back)
    else:  # Backwards
        IO.output(M2_AF, HIGH)   # 2A- (Front)
        IO.output(M2_BF, LOW)  # 2B+ (Front)
        IO.output(M2_AB, HIGH)   # 2A- (Back)
        IO.output(M2_BB, LOW)  # 2B+ (Back)


def FDrive(a, b):
    if b and a:
        rightSide(1)
        leftSide(1)
    elif b:
        rightSide(1)
        leftSide(0)
        #setToHigh([M1_AF, M1_BF, M1_AB, M1_BB])
    elif a:
        rightSide(0)
        leftSide(1)
    else:
        rightSide(0)
        leftSide(0)
     #   setToHigh([M2_AF, M2_BF, M2_AB, M2_BB])


def stop():
    setToHigh(outList)


def drive():
    try:
        while True:

            sen1 = IO.input(IR_L)  # Read Left Sensor
            sen2 = IO.input(IR_R)  # Read Right Sensor

            if(sen1 == HIGH and sen2 == HIGH):  # both while move forward
                FDrive(1, 1)

            elif(sen1 == LOW and sen2 == HIGH):  # turn right
                FDrive(0, 0)
                sleep(0.05)
                FDrive(0, 1)
                print "Turn Right"

            elif(sen1 == HIGH and sen2 == LOW):  # turn left
                FDrive(0, 0)
                sleep(0.05)
                FDrive(1, 0)
                print "Turn Left"

            else:  # stay still
                stop()

    except KeyboardInterrupt:
        print "stop"
        stop()


setOutput(outList)
setInput(inList)
setToHigh(enList)

drive()

exit()

