import RPi.GPIO as IO
from time import sleep
import numpy as np
import cv2


video_capture = cv2.VideoCapture(-1)
video_capture.set(3, 160)
video_capture.set(4, 120)

IO.setwarnings(False)
IO.setmode(IO.BCM)

HIGH = True
LOW = False

PWMs = []

maxPow = 40
turnS = 75


M1_AF = 3  # GPIO 3 -> Motor 1 terminal A Front
M1_BF = 2  # GPIO 2 -> Motor 1 terminal B Front
M2_AF = 17  # GPIO 17 -> Motor 2 terminal A Front
M2_BF = 18  # GPIO 18 -> Motor 2 terminal B Front

M1_AB = 12  # GPIO 12 -> Motor 1 terminal A Back
M1_BB = 13  # GPIO 13 -> Motor 1 terminal B Back
M2_AB = 26  # GPIO 26 -> Motor 2 terminal A Back
M2_BB = 16  # GPIO 16 -> Motor 2 terminal B Back

M1_EnF = 27  # GPIO 27 -> Motor Left Enable Front
M2_EnF = 4   # GPIO 4 -> Motor Right Enable Front
M1_EnB = 5   # GPIO 5 -> Motor Left Enable Back
M2_EnB = 6   # GPIO 6 -> Motor Right Enable Back
# endregion

# region Pin groupings
outList = [M1_AF, M1_BF, M2_AF, M2_BF, M1_AB, M1_BB,
           M2_AB, M2_BB, M1_EnF, M2_EnF, M1_EnB, M2_EnB]
enList = [M1_EnF, M2_EnF, M1_EnB, M2_EnB]
# endregion


def setOutput(lst):
    for i in lst:
        IO.setup(i, IO.OUT)


def setToHigh(lst):
    for i in lst:
        IO.output(i, HIGH)


def setPwm(lst):
    out = []

    for i in range(len(lst)):
        i = IO.PWM(lst[i], 500)
        i.start(0)
        out.append(i)
    
    return out


def rightSide(a):
    if a:  # Forwards
        IO.output(M1_AF, HIGH)  # 1A+ (Front)
        IO.output(M1_BF, LOW)   # 1B- (Front)
        IO.output(M1_AB, HIGH)  # 1A+ (Back)
        IO.output(M1_BB, LOW)   # 1B- (Back)
    else:  # Backwards
        IO.output(M1_AF, LOW)  # 1A+ (Front)
        IO.output(M1_BF, HIGH)   # 1B- (Front)
        IO.output(M1_AB, LOW)  # 1A+ (Back)
        IO.output(M1_BB, HIGH)   # 1B- (Back)


def leftSide(a):
    if a:  # Forwards
        IO.output(M2_AF, LOW)   # 2A- (Front)
        IO.output(M2_BF, HIGH)  # 2B+ (Front)
        IO.output(M2_AB, LOW)   # 2A- (Back)
        IO.output(M2_BB, HIGH)  # 2B+ (Back)
    else:  # Backwards
        IO.output(M2_AF, HIGH)   # 2A- (Front)
        IO.output(M2_BF, LOW)  # 2B+ (Front)
        IO.output(M2_AB, HIGH)   # 2A- (Back)
        IO.output(M2_BB, LOW)  # 2B+ (Back)


def FDrive(a, b, aS = turnS, bS = turnS):

    PWMs[0].ChangeDutyCycle(bS)
    PWMs[1].ChangeDutyCycle(aS)
    PWMs[2].ChangeDutyCycle(bS)
    PWMs[3].ChangeDutyCycle(aS)

    if b and a:
        rightSide(1)
        leftSide(1)
    elif b:
        rightSide(1)
        leftSide(0)
        #setToHigh([M1_AF, M1_BF, M1_AB, M1_BB])
    elif a:
        rightSide(0)
        leftSide(1)
    else:
        rightSide(0)
        leftSide(0)
     #   setToHigh([M2_AF, M2_BF, M2_AB, M2_BB])


def stop():
    setToHigh(outList)


def drive():
    while(True):
        # Capture the frames
        ret, frame = video_capture.read()

        # Crop the image
        crop_img = frame[60:120, 0:160]

        # Convert to grayscale
        gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)

        # Gaussian blur
        blur = cv2.GaussianBlur(gray,(5,5),0)

        # Color thresholding
        ret,thresh = cv2.threshold(blur,60,255,cv2.THRESH_BINARY_INV)

        # Find the contours of the frame
        contours,hierarchy = cv2.findContours(thresh.copy(), 1, cv2.CHAIN_APPROX_NONE)

        # Find the biggest contour (if detected)
        if len(contours) > 0:
            c = max(contours, key=cv2.contourArea)
            M = cv2.moments(c)
            
            if M['m00'] > 0:
                cx = int(M['m10']/M['m00'])
                cy = int(M['m01']/M['m00'])

            cv2.line(crop_img,(cx,0),(cx,720),(255,0,0),1)
            cv2.line(crop_img,(0,cy),(1280,cy),(255,0,0),1)

            cv2.drawContours(crop_img, contours, -1, (0,255,0), 1)

            if cx >= 120:
                FDrive(1,0)
                print "Turn Right!"

            if cx < 120 and cx > 50:
                FDrive(1,1,maxPow,maxPow)
                print "On Track!"

            if cx <= 50:
                FDrive(0,1)
                print "Turn Left"

        else:
            stop()
            print "I don't see the line"

        #Display the resulting frame
#        cv2.imshow('frame0',crop_img)
#        cv2.imshow('frame1',gray)
#        cv2.imshow('frame2',blur)
#        cv2.imshow('frame3',thresh)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            stop()
            break


setOutput(outList)
PWMs = setPwm(enList)


drive()

exit()


