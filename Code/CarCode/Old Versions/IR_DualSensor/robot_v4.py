import RPi.GPIO as IO

IO.setwarnings(False)
IO.setmode(IO.BCM)

HIGH = True
LOW = False

# region Pin config
IR_L = 25  # GPIO 25 -> Left IR out
IR_R = 23  # GPIO 23 -> Right IR out

M1_AF = 3  # GPIO 3 -> Motor 1 terminal A Front
M1_BF = 2  # GPIO 2 -> Motor 1 terminal B Front
M2_AF = 17  # GPIO 17 -> Motor 2 terminal A Front
M2_BF = 18  # GPIO 18 -> Motor 2 terminal B Front

M1_AB = 12  # GPIO 12 -> Motor 1 terminal A Back
M1_BB = 13  # GPIO 13 -> Motor 1 terminal B Back
M2_AB = 26  # GPIO 26 -> Motor 2 terminal A Back
M2_BB = 16  # GPIO 16 -> Motor 2 terminal B Back

M1_EnF = 27  # GPIO 27 -> Motor Left Enable Front
M2_EnF = 4   # GPIO 4 -> Motor Right Enable Front
M1_EnB = 5   # GPIO 5 -> Motor Left Enable Back
M2_EnB = 6   # GPIO 6 -> Motor Right Enable Back
# endregion

# region Pin groupings
outList = [M1_AF, M1_BF, M2_AF, M2_BF, M1_AB, M1_BB, M2_AB, M2_BB, M1_EnF, M2_EnF, M1_EnB, M2_EnB]
inList = [IR_L, IR_R]
enList = [M1_EnF, M2_EnF, M1_EnB, M2_EnB]
# endregion


def setOutput(lst):
    for i in lst:
        IO.setup(i, IO.OUT)


def setInput(lst):
    for i in lst:
        IO.setup(i, IO.IN)


def setToHigh(lst):
    for i in lst:
        IO.output(i, HIGH)


def BDrive():
    IO.output(M1_AB, HIGH)
    IO.output(M1_BB, LOW)
    IO.output(M2_AB, LOW)
    IO.output(M2_BB, HIGH)

def BStop():
    IO.output(M1_AB, HIGH)
    IO.output(M1_BB, HIGH)
    IO.output(M2_AB, HIGH)
    IO.output(M2_BB, HIGH)

def FDrive(a, b):
    if a:
        IO.output(M1_AF, HIGH)  # 1A+
        IO.output(M1_BF, LOW)  # 1B-
    else:
        setToHigh([M1_AF, M1_BF])

    if b:
        IO.output(M2_AF, LOW)  # 2A+
        IO.output(M2_BF, HIGH)
    else:
        setToHigh([M2_AF, M2_BF])


def stop():
    setToHigh(outList)


def drive():
    try:
        while True:

            sen1 = IO.input(IR_L)  # Read Left Sensor
            sen2 = IO.input(IR_R)  # Read Right Sensor

            if(sen1 == HIGH and sen2 == HIGH):  # both while move forward
                FDrive(1, 1)
                BDrive()

            elif(sen1 == LOW and sen2 == HIGH):  # turn right
                BStop()
		FDrive(1, 0)
                #BDrive()

            elif(sen1 == HIGH and sen2 == LOW):  # turn left
                BStop()
		FDrive(0, 1)
                #BDrive()

            else:  # stay still
                stop()

    except KeyboardInterrupt:
        print "stop"
	stop()


setOutput(outList)
setInput(inList)
setToHigh(enList)

drive()

exit()

