import RPi.GPIO as IO

IO.setwarnings(False)
IO.setmode(IO.BCM)

IO.setup(2, IO.IN)  # GPIO 2 -> Left IR out
IO.setup(3, IO.IN)  # GPIO 3 -> Right IR out

IO.setup(4, IO.OUT)  # GPIO 4 -> Motor 1 terminal A
IO.setup(14, IO.OUT)  # GPIO 14 -> Motor 1 terminal B

IO.setup(22, IO.OUT)  # GPIO 22 -> Motor Left terminal A
IO.setup(23, IO.OUT)  # GPIO 23 -> Motor Left terminal B

while 1:
    
    sen1 = IO.input(2)
    sen2 = IO.input(3)
    
    print "Ben 2: %s\tBen 3: %s" % (sen1,sen2)

    if(sen1 == True and sen2 == True):  # both while move forward
        IO.output(4, True)  # 1A+
        IO.output(14, False)  # 1B-

        IO.output(22, True)  # 2A+
        IO.output(23, False)  # 2B-

    elif(sen1 == False and sen2 == True):  # turn right
        IO.output(4, True)  # 1A+
        IO.output(14, True)  # 1B-

        IO.output(22, True)  # 2A+
        IO.output(23, False)  # 2B-

    elif(sen1 == True and sen2 == False):  # turn left
        IO.output(4, True)  # 1A+
        IO.output(14, False)  # 1B-

        IO.output(22, True)  # 2A+
        IO.output(23, True)  # 2B-

    else:  # stay still
        IO.output(4, True)  # 1A+
        IO.output(14, True)  # 1B-

        IO.output(22, True)  # 2A+
        IO.output(23, True)  # 2B-
