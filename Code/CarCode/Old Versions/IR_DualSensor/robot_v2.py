import RPi.GPIO as IO

IO.setwarnings(False)
IO.setmode(IO.BCM)

IR_L = 25
IR_R = 23
#                                                                                                           Front
M1_AF = 3  # +----------------------+
M1_BF = 2  # Motor 1  +------>   |                      |   <------+  Motor 2
#                                                                                                  |                      |
M2_AF = 17  # |                      |
M2_BF = 18  # |                      |
#                                                                                                  |                      |
M1_AB = 12  # |                      |
M1_BB = 13  # |                      |
#                                                                                                  |                      |
M2_AB = 26  # |                      |
M2_BB = 16  # |                      |
#                                                                                                  |                      |
M1_EnF = 27  # |                      |
M2_EnF = 4  # |                      |
M1_EnB = 5  # |                      |
M2_EnB = 6  # |                      |
#                                                                              Motor 1  +------>   |                      |   <-------+  Motor 2
#                                                                                                  +----------------------+
#                                                                                                            Back
IO.setup(IR_L, IO.IN)  # GPIO 10 -> Left IR out
IO.setup(IR_R, IO.IN)  # GPIO 23 -> Right IR out

IO.setup(M1_AF, IO.OUT)  # GPIO 3 -> Motor 1 terminal A Front
IO.setup(M1_BF, IO.OUT)  # GPIO 2 -> Motor 1 terminal B Front

IO.setup(M2_AF, IO.OUT)  # GPIO 17 -> Motor 2 terminal A Front
IO.setup(M2_BF, IO.OUT)  # GPIO 18 -> Motor 2 terminal B Front

IO.setup(M1_AB, IO.OUT)  # GPIO 12 -> Motor 1 terminal A Back
IO.setup(M1_BB, IO.OUT)  # GPIO 13 -> Motor 1 terminal B Back

IO.setup(M2_AB, IO.OUT)  # GPIO 26 -> Motor 2 terminal A Back
IO.setup(M2_BB, IO.OUT)  # GPIO 16 -> Motor 2 terminal B Back


IO.setup(M1_EnF, IO.OUT)  # GPIO 27 -> Motor Left Enable Front
IO.setup(M2_EnF, IO.OUT)  # GPIO 4 -> Motor Right Enable Front
IO.setup(M1_EnB, IO.OUT)  # GPIO 5 -> Motor Left Enable Back
IO.setup(M2_EnB, IO.OUT)  # GPIO 6 -> Motor Right Enable Back


HIGH = True
LOW = False

while True:

    # Sets all enable pins to high
    IO.output(M1_EnF, HIGH)
    IO.output(M2_EnF, HIGH)
    IO.output(M1_EnB, HIGH)
    IO.output(M2_EnB, HIGH)

    sen1 = IO.input(IR_L) # Read Left Sensor
    sen2 = IO.input(IR_R) # Read Right Sensor

    if(sen1 == HIGH and sen2 == HIGH):  # both while move forward
        IO.output(M1_AF, HIGH)  # 1A+
        IO.output(M1_BF, LOW)  # 1B-

        IO.output(M2_AF, LOW)  # 2A+
        IO.output(M2_BF, HIGH)  # 2B-

    elif(sen1 == LOW and sen2 == HIGH):  # turn right
        IO.output(M1_AF, HIGH)  # 1A+
        IO.output(M1_BF, HIGH)  # 1B-

        IO.output(M2_AF, HIGH)  # 2A+
        IO.output(M2_BF, LOW)  # 2B-

    elif(sen1 == HIGH and sen2 == LOW):  # turn left
        IO.output(M1_AF, HIGH)  # 1A+
        IO.output(M1_BF, LOW)  # 1B-

        IO.output(M2_AF, HIGH)  # 2A+
        IO.output(M2_BF, HIGH)  # 2B-

    else:  # stay still
        IO.output(M1_AF, HIGH)  # 1A+
        IO.output(M1_BF, HIGH)  # 1B-

        IO.output(M2_AF, HIGH)  # 2A+
        IO.output(M2_BF, HIGH)  # 2B-

