import RPi.GPIO as GPIO                              

GPIO.setwarnings(False)                              
GPIO.setmode(GPIO.BOARD)

GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  #Left_Sensor
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  #Right_Sensor

#Back_Right motor                              
GPIO.setup(29, GPIO.OUT, initial=GPIO.HIGH)  #1,2 Enable
GPIO.setup(32, GPIO.OUT, initial=GPIO.HIGH)  #In 1
GPIO.setup(33, GPIO.OUT, initial=GPIO.LOW)  #In 2

#Back_Left motor
GPIO.setup(31, GPIO.OUT, initial=GPIO.HIGH)  #3,4 Enable
GPIO.setup(37, GPIO.OUT, initial=GPIO.LOW)  #In 3
GPIO.setup(36, GPIO.OUT, initial=GPIO.HIGH)  #In 4

#Front_Left motor
GPIO.setup(13, GPIO.OUT, initial=GPIO.HIGH)  #1,2 Enable
GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW)  #In 1
GPIO.setup(12, GPIO.OUT, initial=GPIO.HIGH)  #In 2

#Front_Right motor   
GPIO.setup(7, GPIO.OUT, initial=GPIO.HIGH)  #3,4 Enable
GPIO.setup(5, GPIO.OUT, initial=GPIO.HIGH)  #In 3
GPIO.setup(3, GPIO.OUT, initial=GPIO.LOW)  #In 4  

def Back_motors():
    if GPIO.input(16) and GPIO.input(22):
      GPIO.output(29, GPIO.LOW)
      GPIO.output(31, GPIO.LOW)
    if GPIO.input(16) == GPIO.LOW or GPIO.input(22) == GPIO.LOW:
      GPIO.output(29, GPIO.HIGH)
      GPIO.output(31, GPIO.HIGH)
      
def Front_motors():
    if GPIO.input(22):
      GPIO.output(13, GPIO.LOW)
    if GPIO.input(16):
      GPIO.output(7, GPIO.LOW)
    if GPIO.input(22) == GPIO.LOW:
      GPIO.output(13, GPIO.HIGH)
    if GPIO.input(16) == GPIO.LOW:
      GPIO.output(7, GPIO.HIGH)
      


def main():
  while True:
    Back_motors()
    Front_motors()  

  



main()