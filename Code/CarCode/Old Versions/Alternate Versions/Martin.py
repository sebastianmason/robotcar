import RPi.GPIO as GPIO                               

GPIO.setwarnings(False)                              
GPIO.setmode(GPIO.BOARD)

GPIO.setup(22, GPIO.IN)  #Left sensor
GPIO.setup(16, GPIO.IN)  #Right sensor
 
#Back_right motor                              
GPIO.setup(29, GPIO.OUT, initial=GPIO.LOW)  #1,2 Enable
GPIO.setup(32, GPIO.OUT, initial=GPIO.HIGH)  #In 1
GPIO.setup(33, GPIO.OUT, initial=GPIO.LOW)  #In 2
   
#Front_right motor
GPIO.setup(7, GPIO.OUT, initial=GPIO.LOW)  #3,4 Enable
GPIO.setup(5, GPIO.OUT, initial=GPIO.HIGH)  #In 3
GPIO.setup(3, GPIO.OUT, initial=GPIO.LOW)  #In 4

#Back_left motor                              
GPIO.setup(31, GPIO.OUT, initial=GPIO.LOW)  #1,2 Enable
GPIO.setup(37, GPIO.OUT, initial=GPIO.LOW)  #In 1
GPIO.setup(36, GPIO.OUT, initial=GPIO.HIGH)  #In 2
   
#Front_left motor
GPIO.setup(13, GPIO.OUT, initial=GPIO.LOW)  #3,4 Enable
GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW)  #In 3
GPIO.setup(12, GPIO.OUT, initial=GPIO.HIGH)  #In 4
      

while True:
  if GPIO.input(16) and GPIO.input(22):
    GPIO.output(29, GPIO.HIGH)
    GPIO.output(7, GPIO.HIGH)
    GPIO.output(31, GPIO.HIGH)
    GPIO.output(13, GPIO.HIGH)
  else:
    GPIO.output(29, GPIO.LOW)
    GPIO.output(7, GPIO.LOW)
    GPIO.output(31, GPIO.LOW)
    GPIO.output(13, GPIO.LOW)
 