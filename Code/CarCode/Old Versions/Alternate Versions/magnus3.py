#!/usr/bin/python
import RPi.GPIO as IO  # import GPIO library
import time  # Import time library
IO.setwarnings(False)  # Remove warnings
IO.setmode(IO.BCM)  # Using GPIO numbering, NOT board numbering


# Sensors
IO.setup(25, IO.IN)  # IR 1
IO.setup(23, IO.IN)  # IR 2

# Motercontroller 1
IO.setup(5, IO.OUT)  # Enable 1,2
IO.setup(6, IO.OUT)  # Enable 3,4
IO.setup(12, IO.OUT)  # In 1
IO.setup(13, IO.OUT)  # In 2
IO.setup(26, IO.OUT)  # In 3
IO.setup(16, IO.OUT)  # In 4

# Motorcontroller 2
IO.setup(27, IO.OUT)  # Enable 1,2
IO.setup(4, IO.OUT)  # Enable 3,4
IO.setup(17, IO.OUT)  # In 1
IO.setup(18, IO.OUT)  # In 2
IO.setup(3, IO.OUT)  # In 3
IO.setup(2, IO.OUT)  # In 4

HIGH = True
LOW = False


def drive():
    IO.output(12, HIGH)
    IO.output(13, LOW)
    IO.output(26, LOW)
    IO.output(16, HIGH)
    IO.output(17, LOW)
    IO.output(18, HIGH)
    IO.output(3, HIGH)
    IO.output(2, LOW)


def stop():
    IO.output(12, LOW)
    IO.output(13, LOW)
    IO.output(26, LOW)
    IO.output(16, LOW)
    IO.output(17, LOW)
    IO.output(18, LOW)
    IO.output(3, LOW)
    IO.output(2, LOW)


def left():
    IO.output(12, HIGH)
    IO.output(13, LOW)
    IO.output(26, LOW)
    IO.output(16, LOW)
    IO.output(17, LOW)
    IO.output(18, LOW)
    IO.output(3, HIGH)
    IO.output(2, LOW)


def right():
    IO.output(12, LOW)
    IO.output(13, LOW)
    IO.output(26, LOW)
    IO.output(16, HIGH)
    IO.output(17, LOW)
    IO.output(18, HIGH)
    IO.output(3, LOW)
    IO.output(2, LOW)


try:

    while True:
        IO.output(5, HIGH)
        IO.output(6, HIGH)
        IO.output(27, HIGH)
        IO.output(4, HIGH)

        if IO.input(25) == HIGH and IO.input(23) == HIGH:
            drive()
        elif IO.input(25) == HIGH and IO.input(23) == LOW:
            right()
        elif IO.input(25) == LOW and IO.input(23) == HIGH:
            left()
        elif IO.input(25) == LOW and IO.input(23) == LOW:
            stop()

except KeyboardInterrupt:
    IO.cleanup()

